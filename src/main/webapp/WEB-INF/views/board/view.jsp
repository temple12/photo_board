<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<jsp:include page="/WEB-INF/commons/head.jsp"></jsp:include>
</head>
<body>
<jsp:include page="/WEB-INF/commons/menu.jsp"></jsp:include>
<form method="POST" id="viewForm" enctype="multipart/form-data">
<input type="hidden" name="pbNum" value="${param.pbNum}">
	<div class="container" id="img"></div>
	<div class="form-group container">
		<b><label for="exampleFormControlFile">사진변경</label> <input type="file"
			class="form-control" id="pbImgPath" name="pbImgPath"></b>
	</div>
		<div class="form-group container" id="content"></div>
</form>
	<div class="container">
		<button type="button" class="btn btn-info" onclick="doUpdate()">수정하기</button>
		<button type="button" class="btn btn-info" onclick="doDelete()">삭제하기</button>
		<a href="/views/board/list"><button type="button" class="btn btn-secondary">목록으로</button></a>
	</div>
	<script>
		$(document).ready(function() {
				var viewConf = {
					method : 'GET',
					url : '/view',
					data : {
						pbNum : ${param.pbNum}
					},
					success : function(res) {
						var html = '';
						var html2 = '';
						console.log(res);
						if(res.pbImgPath != null){
							html += '<img src="/resources/image/'+res.pbImgPath+'"width="70%"/>';
							html += '<input type="hidden" name="pbImgPath" id="pbImgPath" value="'+res.pbImgPath+'">';
						}else{
							html += '<h3>등록된 사진이 없습니다.</h3>';
							html += '<h3>아래 파일선택으로 사진을 추가해주세요.</h3>';
						}
						html2 += '<b><label for="exampleFormControlInput">제목</label></b>'; 
						html2 += '<input type="text" class="form-control" id="pbTitle" name="pbTitle" value="'+res.pbTitle+'">';
						html2 += '<b><label for="exampleFormControlTextarea">내용</label></b>';
						html2 += '<textarea class="form-control" id="pbContent" name="pbContent" rows="10">'+res.pbContent+'</textarea>';
						document.querySelector('#img').innerHTML += html;
						document.querySelector('#content').innerHTML += html2;
					},
					error : function(error) {

					}
				}
				$.ajax(viewConf);
		});
		
		function doDelete() {
			var formData = new FormData($('#viewForm')[0]);
			var deleteConf = {
				method : 'POST',
				enctype : 'multipart/form-data',
				url : '/delete',
				data : formData,
				processData : false,
				contentType : false,
				success : function(res) {
					if (res === 1) {
						alert('글삭제 성공');
						location.href = '/views/board/list';
					} else {
						alert('글삭제 실패');
					}
				},
				error : function(error) {

				}
			}
			$.ajax(deleteConf);
		}
		
		function doUpdate() {
			var formData = new FormData($('#viewForm')[0]);
			var updateConf = {
				method : 'POST',
				enctype : 'multipart/form-data',
				url : '/update',
				data : formData,
				processData : false,
				contentType : false,
				success : function(res) {
					if (res === 1) {
						alert('글수정 성공');
						location.href = '/views/board/list';
					} else {
						alert('글수정 실패');
					}
				},
				error : function(error) {

				}
			}
			$.ajax(updateConf);
		}
	</script>
</body>
</html>