<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<jsp:include page="/WEB-INF/commons/head.jsp"></jsp:include>
<style type="text/css">
h1 {
	text-align: center;
}
</style>
</head>
<body>
<jsp:include page="/WEB-INF/commons/menu.jsp"></jsp:include>
	<h1>글작성</h1>
	<form method="POST" id="uploadForm" enctype="multipart/form-data">
		<div class="form-group">
			<label for="exampleFormControlInput">제목</label> <input type="text"
				class="form-control" id="pbTitle" name="pbTitle"
				placeholder="제목을 작성해주세요.">
		</div>
		<div class="form-group">
			<label for="exampleFormControlTextarea">내용</label>
			<textarea class="form-control" id="pbContent"
				name="pbContent" rows="15"></textarea>
		</div>
		<div class="form-group">
			<label for="exampleFormControlFile">사진파일</label> <input type="file"
				class="form-control" id="pbImgPath" name="pbImgPath">
		</div>
	</form>
		<button type="button" class="btn btn-info" onclick="doInsert()">등록하기</button>
		<a href="/views/board/list"><button type="button" class="btn btn-secondary">목록으로</button></a>
	<script>
		function doInsert() {
			var formData = new FormData($('#uploadForm')[0]);
			var conf = {
				method : 'POST',
				enctype : 'multipart/form-data',
				url : '/insert',
				data : formData,
				processData : false,
				contentType : false,
				success : function(res) {
					if (res === 1) {
						alert('글작성 성공');
						location.href = '/views/board/list';
					} else {
						aelrt('글작성 실패');
					}
				},
				error : function(error) {

				}
			}
			$.ajax(conf);
		}
	</script>
</body>
</html>