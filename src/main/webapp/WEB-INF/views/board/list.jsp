<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<jsp:include page="/WEB-INF/commons/head.jsp"></jsp:include>
<link rel="icon" type="image/png" href="http://example.com/myicon.png">
<style>
h1 {
	text-align: center;
}
</style>
</head>
<body>
<jsp:include page="/WEB-INF/commons/menu.jsp"></jsp:include>
	<h1>게시판</h1><br>
	<script>
		$(document).ready(getList);
		function getList(pageNum) {
			if(isNaN(pageNum)){
				pageNum = 1;
			}
			$.ajax({
					url : '/list',
					method : 'GET',
					data : {
						pageNum : pageNum,
					},
					success : function(res) {
						var html = '';		
						for (var i = 0; i < res[0].pbList.length; i++) {						
							var pbList = res[0].pbList[i];
							html +=  '<tr onclick="goView(' + pbList.pbNum + ')">';
							$('th[data-col]').each(function(idx, th) {
								var col = th.getAttribute('data-col');
								if(col === 'pbImgPath'){
									if(pbList.pbImgPath != null){
										html += '<td><img src="/resources/image/'+pbList[col]+'"width="200px""/></td>';
									}
									if(pbList.pbImgPath == null){
										html += '<td></td>';
									}
								}else{
									html += '<td>' + pbList[col] + '</td>';
								}
								
							})
							html += '</tr>';
						}
						$('#tbody').html(html);
						var page = res[0].page;
						var pageHtml = '<nav aria-label="..." id="page">';
						pageHtml += '<ul class="pagination pagination-lg justify-content-center">';
						if(res[0].page.pageNum!=1){
						pageHtml += '<li class="page-item"><a class="page-link" href="#" onclick="getList()">첫페이지</a></li>';
							pageHtml += '<li class="page-item"><a class="page-link" href="#" aria-label="Previous" onclick="getList(' + (page.pageNum-1) + ')">';
					   		pageHtml += '<span aria-hidden="true">&laquo;</span></a></li>';
						}else{
							pageHtml += '<li class="page-item disabled"> <a class="page-link" href="#" tabindex="-1" aria-disabled="true">첫페이지</a></li>';
						}
						for(var i=page.startBlock;i<=page.endBlock;i++){
							if(page.pageNum==i){
								pageHtml += '<li class="page-item active" aria-current="page"><span class="page-link">' + i + '<span class="sr-only">(current)</span></span></li>';
							}else{
								pageHtml += '<li class="page-item"><a class="page-link" href="#" onclick="getList('+ i +')">' + i + '</a></li>';
							}
						}
						if(page.totalPageSize!=page.pageNum){
					   		pageHtml += '<li class="page-item"><a class="page-link" href="#" aria-label="Next" onclick="getList('+ (page.pageNum+1) +')">';
					    	pageHtml += '<span aria-hidden="true">&raquo;</span></a></li>';
					    	pageHtml += '<li class="page-item"><a class="page-link" href="#" onclick="getList(' + page.totalPageSize + ')">마지막페이지</a></li>';
						}else{
							pageHtml += '<li class="page-item disabled"> <a class="page-link" href="#" tabindex="-1" aria-disabled="true">마지막페이지</a></li>';
						}
						pageHtml += '</ul>';
						pageHtml += '</nav>';
						$('#pageDiv').html(pageHtml);
					}
				})
		}
		function goView(pbNum) {
			location.href = '/views/board/view?pbNum=' + pbNum;
		}
	</script>
	<table class="table table-bordered">
		<tr>
			<th data-col="pbNum">번호</th>
			<th data-col="pbTitle">제목</th>
			<th data-col="pbImgPath">사진</th>
			<th data-col="credat">생성일</th>
		</tr>
		<tbody id="tbody">
		</tbody>
	</table>
	<div id="pageDiv"></div>	
</body>
</html>