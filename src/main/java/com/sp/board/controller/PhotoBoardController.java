package com.sp.board.controller;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.sp.board.service.PhotoBoardService;
import com.sp.board.vo.PageVO;
import com.sp.board.vo.PhotoBoardVO;

@Controller
public class PhotoBoardController {
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	@Autowired
	private PhotoBoardService pbService;
	
	@PostMapping("/insert")
	public @ResponseBody int insertPhotoBoard(PhotoBoardVO pbVO, @RequestParam("pbImgPath") MultipartFile file){
		logger.info("pbVO=>{}", pbVO);
		return pbService.insertPhotoBoard(pbVO, file);
	}
	
	@GetMapping("/list")
	public @ResponseBody List<Map<String,Object>> selectPhotoBoardList(PhotoBoardVO pbVO, @RequestParam int pageNum){			
		if(pageNum == 1) {
			pbVO.setPage(new PageVO());
			pbVO.getPage().setPageNum(1);
		}else {
			pbVO.setPage(new PageVO());
			pbVO.getPage().setPageNum(pageNum);
		}
		List<Map<String,Object>> list = pbService.selectPhotoBoardList(pbVO);
		return list;
	}
	
	@GetMapping("/view")
	public @ResponseBody PhotoBoardVO selectPhotoBoard(PhotoBoardVO pbVO){
		return pbService.selectPhotoBoard(pbVO);
	}
	
	@PostMapping("/delete")
	public @ResponseBody int deletePhotoBoard(PhotoBoardVO pbVO, @RequestParam("pbImgPath") MultipartFile file){
		return pbService.deletePhotoBoard(pbVO, file);
	}
	
	@PostMapping("/update")
	public @ResponseBody int updatePhotoBoard(PhotoBoardVO pbVO, @RequestParam("pbImgPath") MultipartFile file){
		return pbService.updatePhotoBoard(pbVO, file);
	}
}
