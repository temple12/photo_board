package com.sp.board.controller.common;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import com.sp.board.controller.HomeController;

@Controller
public class URLController {
	
	@GetMapping("/views/**")
	public String goPage(HttpServletRequest req) {
		String uri = req.getRequestURI();
		return uri.substring(7);
	}

}
