package com.sp.board.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.sp.board.dao.PhotoBoardDAO;
import com.sp.board.service.PhotoBoardService;
import com.sp.board.vo.PageVO;
import com.sp.board.vo.PhotoBoardVO;

@Service
public class PhotoBoardServiceImpl implements PhotoBoardService {
	@Autowired
	private PhotoBoardDAO pbDAO;

	private final String UPLOADPATH = "C:\\Users\\Administrator\\git\\photo_board\\src\\main\\webapp\\resources\\image\\";

	@Override
	public int insertPhotoBoard(PhotoBoardVO pbVO, MultipartFile file) {
		String originFileName = file.getOriginalFilename();
		if (originFileName.equals(null) || originFileName.equals("")) {
			int result = pbDAO.insertPhotoBoard(pbVO);
			return result;
		}
		String extName = originFileName.substring(originFileName.lastIndexOf("."));
		String fileName = System.nanoTime() + extName;
		pbVO.setPbImgPath(fileName);
		int result = pbDAO.insertPhotoBoard(pbVO);
		if (result == 1) {
			File f = new File(UPLOADPATH + fileName);
			try {
				file.transferTo(f);
			} catch (IllegalStateException | IOException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	@Override
	public int updatePhotoBoard(PhotoBoardVO pbVO, MultipartFile file) {
		if (pbVO.getPbImgPath() == null) {
			String originFileName = file.getOriginalFilename();
			String extName = originFileName.substring(originFileName.lastIndexOf("."));
			String fileName = System.nanoTime() + extName;
			pbVO.setPbImgPath(fileName);
			int result = pbDAO.updatePhotoBoard(pbVO);
			if (result == 1) {
				File f = new File(UPLOADPATH + fileName);
				try {
					file.transferTo(f);
				} catch (IllegalStateException | IOException e) {
					e.printStackTrace();
				}
			}
			return result;
		}else {
			String fileNme = pbVO.getPbImgPath();
			File f = new File(UPLOADPATH + fileNme);
			if (f.exists()) {
				f.delete();
			}
			
			String originFileName = file.getOriginalFilename();
			String extName = originFileName.substring(originFileName.lastIndexOf("."));
			String fileName = System.nanoTime() + extName;
			pbVO.setPbImgPath(fileName);
			int result = pbDAO.updatePhotoBoard(pbVO);
			if (result == 1) {
				File newfile = new File(UPLOADPATH + fileName);
				try {
					file.transferTo(newfile);
				} catch (IllegalStateException | IOException e) {
					e.printStackTrace();
				}
			}
			return result;
		}
	}

	@Override
	public int deletePhotoBoard(PhotoBoardVO pbVO, MultipartFile file) {
		int result = pbDAO.deletePhotoBoard(pbVO);
		if (result == 1) {
			String fileNme = pbVO.getPbImgPath();
			File f = new File(UPLOADPATH + fileNme);
			if (f.exists()) {
				f.delete();
			}
		}
		return result;
	}

	@Override
	public List<Map<String,Object>> selectPhotoBoardList(PhotoBoardVO pbVO) {
		PageVO page = pbVO.getPage();
		int startNum = (page.getPageNum() - 1) * 10 + 1;
		int endNum = startNum + (10 - 1);
		page.setStartNum(startNum);
		page.setEndNum(endNum);
		int totalCnt = pbDAO.selectPhotoBoardCount(pbVO);
		int totalPageSize = totalCnt / 10;
		if (totalCnt % 10 != 0) {
			totalPageSize += 1;
		}
		page.setTotalPageSize(totalPageSize);
		int startBlock = (page.getPageNum() - 1) / 10 * 10 + 1;
		int endBlock = startBlock + (10 - 1);
		if (endBlock > totalPageSize) {
			endBlock = totalPageSize;
		}
		page.setStartBlock(startBlock);
		page.setEndBlock(endBlock);
		Map<String,Object> map = new HashMap<>();
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		map.put("page", pbVO.getPage());
		map.put("pbList", pbDAO.selectPhotoBoardList(pbVO));
		list.add(map);
		
		return list;
	}

	@Override
	public PhotoBoardVO selectPhotoBoard(PhotoBoardVO pbVO) {
		return pbDAO.selectPhotoBoard(pbVO);
	}

}
