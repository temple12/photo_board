package com.sp.board.service;

import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.sp.board.vo.PhotoBoardVO;

public interface PhotoBoardService {
	int insertPhotoBoard(PhotoBoardVO pbVO, MultipartFile file);
	int updatePhotoBoard(PhotoBoardVO pbVO, MultipartFile file);
	int deletePhotoBoard(PhotoBoardVO pbVO, MultipartFile file);
	List<Map<String,Object>> selectPhotoBoardList(PhotoBoardVO pbVO);
	PhotoBoardVO selectPhotoBoard(PhotoBoardVO pbVO);
}
