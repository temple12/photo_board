package com.sp.board.vo;

import org.apache.ibatis.type.Alias;

import lombok.Data;

@Data
@Alias("pb")
public class PhotoBoardVO {
	private int pbNum;
	private String pbTitle;
	private String pbContent;
	private String pbImgPath;
	private String credat;
	private String cretim;
	private PageVO page;
}
