package com.sp.board.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import com.sp.board.dao.PhotoBoardDAO;
import com.sp.board.vo.PhotoBoardVO;

@Repository
public class PhotoBoardDAOImpl implements PhotoBoardDAO {
	@Autowired
	private SqlSessionFactory ssf;
	
	@Override
	public int insertPhotoBoard(PhotoBoardVO pbVO) {
		try(SqlSession ss = ssf.openSession()){
			return ss.insert("PhotoBoard.insertPhotoBoard",pbVO);
		}
	}

	@Override
	public int updatePhotoBoard(PhotoBoardVO pbVO) {
		try(SqlSession ss = ssf.openSession()){
			return ss.update("PhotoBoard.updatePhotoBoard",pbVO);
		}
	}

	@Override
	public int deletePhotoBoard(PhotoBoardVO pbVO) {
		try(SqlSession ss = ssf.openSession()){
			return ss.delete("PhotoBoard.deletePhotoBoard",pbVO);
		}
	}

	@Override
	public List<Map<String,Object>> selectPhotoBoardList(PhotoBoardVO pbVO) {
		try(SqlSession ss = ssf.openSession()){
			return ss.selectList("PhotoBoard.selectPhotoBoardList",pbVO);
		}
	}

	@Override
	public PhotoBoardVO selectPhotoBoard(PhotoBoardVO pbVO) {
		try(SqlSession ss = ssf.openSession()){
			return ss.selectOne("PhotoBoard.selectPhotoBoard",pbVO);
		}
	}

	@Override
	public int selectPhotoBoardCount(PhotoBoardVO pbVO) {
		try(SqlSession ss = ssf.openSession()){
			return ss.selectOne("PhotoBoard.selectPhotoBoardCount", pbVO);
		}
	}

}
