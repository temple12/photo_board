package com.sp.board.dao;

import java.util.List;
import java.util.Map;

import com.sp.board.vo.PhotoBoardVO;

public interface PhotoBoardDAO {
	int insertPhotoBoard(PhotoBoardVO pbVO);
	int updatePhotoBoard(PhotoBoardVO pbVO);
	int deletePhotoBoard(PhotoBoardVO pbVO);
	List<Map<String,Object>> selectPhotoBoardList(PhotoBoardVO pbVO);
	PhotoBoardVO selectPhotoBoard(PhotoBoardVO pbVO);
	int selectPhotoBoardCount(PhotoBoardVO pbVO);
}
