package com.sp.board;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.apache.commons.dbcp2.BasicDataSource;
import org.junit.runner.RunWith;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="file:src\\main\\webapp\\WEB-INF\\spring\\root-context.xml")
public class Test {
	@Autowired
	private BasicDataSource bds;
	
	@Autowired
	private SqlSessionFactoryBean ssf;
	
	@org.junit.Test
	public void test() {
		assertThat(ssf, is(notNullValue()));
	}

}
